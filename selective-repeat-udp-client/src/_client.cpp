#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <string>
#include <iostream>
#include <signal.h>
#include <time.h>

#include <errno.h>      /* for errno and EINTR */

#define MAX_PACKET_SIZE 		512     /* Longest string to echo */
#define ACK_PACKET_SIZE			8
#define CHUNK_SIZE 				504
#define FIN						2000
#define TIMEOUT					2

#define ACK						1
#define NOTACK					2
#define NOTPCK					3

using namespace std;

struct packet {
	/* Header */
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t seqno;
	/* Data */
	char data[CHUNK_SIZE]; /* Not always 500 bytes, can be less */
};

/* Ack-only packets are only 8 bytes */
struct ack_packet {
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t ackno;
};

void die_with_error(const char* error_message) {
	printf("%s\n", error_message);
}

void catch_alarm(int ignored) {
	printf("alarm time out occurred\n");
}

bool inside_window(uint num, uint base, uint end) {
	if (base < end) {
		if (num >= base && num <= end)
			return true;
		else
			return false;
	} else if (base > end) {
		if (num >= base || num <= end)
			return true;
		else
			return false;
	} else {
		return false;
	}
}

int main(int argc, char *argv[]) {
	// timer initialization
	struct sigaction myAction; /* For setting signal handler */
	/* Set signal handler for alarm signal */
	myAction.sa_handler = catch_alarm;
	if (sigfillset(&myAction.sa_mask) < 0) /* block everything in handler */
		die_with_error("sigfillset() failed");
	myAction.sa_flags = 0;

	sigaction(SIGALRM, &myAction, 0);

	int sock; /* Socket descriptor */
	struct sockaddr_in server_addr; /* Echo server address */
	struct sockaddr_in from_addr; /* Source address of echo */
	unsigned short server_port; /* Echo server port */
	unsigned int from_size; /* In-out of address size for recvfrom() */
	char *server_ip; /* IP address of server */
	char *filename; /* File name to send to server */
	int filename_len; /* Length of filename */

	uint window_size = 5;
	// array which contains the received data packets
	packet ** _buffer = new packet*[2 * window_size];
	// array which tell if a packet is ack or not yet
	int ack_window[2 * window_size];

	server_ip = (char*) "127.0.0.1";
	server_port = 8080;

	filename = (char*) "test.txt";

	if ((filename_len = strlen(filename)) > CHUNK_SIZE) /* Check input length */
		die_with_error("file name too long");

	packet *filename_packet = new packet;

	//set filename
	strncpy(filename_packet->data, filename, filename_len);
	filename_packet->len = filename_len;
	filename_packet->seqno = 0; // let seqno = 0 be the first name packet
	//TODO checksum
//	filename_packet->cksum;

	/* Create a datagram/UDP socket */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		die_with_error("socket() failed");

	/* Construct the server address structure */
	memset(&server_addr, 0, sizeof(server_addr)); /* Zero out structure */
	server_addr.sin_family = AF_INET; /* Internet addr family */
	server_addr.sin_addr.s_addr = inet_addr(server_ip); /* Server IP address */
	server_addr.sin_port = htons(server_port); /* Server port */

	/* Send the string to the server */
	if (sendto(sock, filename_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &server_addr,
			sizeof(server_addr)) != MAX_PACKET_SIZE)
		die_with_error("sendto() failed with wrong bytes sent");

	alarm(TIMEOUT);

	/* Recv a response */
	from_size = sizeof(from_addr);

	packet *data_packet = new packet;
	uint base = 0;
	int packet_size = 0;

	//open new file
	FILE* fp = fopen(filename, "w");

	while ((packet_size = recvfrom(sock, data_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &from_addr, &from_size)) < 0) {
		if (errno == EINTR) {
			cout << "Time out, resend file name to the server" << endl;
			sendto(sock, filename_packet, MAX_PACKET_SIZE, 0,
					(struct sockaddr *) &server_addr, sizeof(server_addr));
			// reinitialize the timer
			alarm(TIMEOUT);
		}
	}

	bool file_finished = false;
	uint seq_finish = 0;

	do {
		alarm(0);

		if (data_packet->len == FIN) {
			file_finished = true;
			seq_finish = data_packet->seqno % (2 * window_size);
		}

		uint num = (data_packet->seqno) % (2 * window_size);
		// means that the packet is in the range of the window
		uint end = (base + window_size) % (2 * window_size);

		ack_packet* ack = new ack_packet;

		if (inside_window(num, base, end)) {
			cout << "data: " << data_packet->data << endl;
			cout << "data seqno: " << data_packet->seqno << endl;
			cout << "data length: " << data_packet->len << endl;

			_buffer[num] = data_packet;
			ack_window[num] = ACK;

			// send +ve ack with the received seqno
			ack->ackno = data_packet->seqno;
			sendto(sock, ack, ACK_PACKET_SIZE, 0,
					(struct sockaddr *) &server_addr, sizeof(server_addr));

			if (num == base) {
				int counter = base;

				while (ack_window[counter] == ACK
						&& inside_window(counter, base, end)) {

					if (_buffer[counter]->len != FIN) {
						//write  current chunk to file
						if ((fwrite(_buffer[counter]->data, sizeof(char),
								_buffer[counter]->len, fp)) < 0) {
							die_with_error("Error writing to file");
						}
					}

					counter = (counter + 1) % (2 * window_size);
				}
				// update the base
				base = counter;
				printf("New base %d", base);
			}
		} else if (inside_window(num, base - window_size, base - 1)) {
			cout << "duplicate data seqno: " << data_packet->seqno << endl;

			// duplicate packets
			ack_window[num] = NOTPCK;
			// send +ve ack with the received seqno
			ack->ackno = data_packet->seqno;

			sendto(sock, ack, ACK_PACKET_SIZE, 0,
					(struct sockaddr *) &server_addr, sizeof(server_addr));
		} else {
			// ignore the packet
		}

		cout << endl;

		if (file_finished == true && base == seq_finish + 1) {
			break;
		}
	} while ((packet_size = recvfrom(sock, data_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &from_addr, &from_size)) > 0);

	// close file
	fclose(fp);

	if (packet_size < 0) {
		die_with_error("Error in reading datagram");
	}

	if (server_addr.sin_addr.s_addr != from_addr.sin_addr.s_addr) {
		fprintf(stderr, "Error: received a packet from unknown source.\n");
		exit(1);
	}

	close(sock);
	exit(0);
}
