#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <iostream>

#include <errno.h>      /* for errno and EINTR */
#include <signal.h>     /* for sigaction() */
#include <time.h>

#define MAX_PACKET_SIZE                 512     /* Longest string to echo */
#define ACK_PACKET_SIZE                 8
#define CHUNK_SIZE                      504
#define FIN                             2000
#define TIMEOUT                         2

using namespace std;

/* Data-only packets */
struct packet {
	/* Header */
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t seqno;
	/* Data */
	char data[CHUNK_SIZE];
};

/* Ack-only packets are only 8 bytes */
struct ack_packet {
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t ackno;
};

void die_with_error(const char *error_message) {
	printf("%s", error_message);
}

void catch_alarm(int ignored) /* Handler for SIGALRM */
{
	printf("alarm occurred\n");
}

int main(int argc, char *argv[]) {
	// timer initialization
	struct sigaction myAction; /* For setting signal handler */

	/* Set signal handler for alarm signal */
	myAction.sa_handler = catch_alarm;
	if (sigfillset(&myAction.sa_mask) < 0) /* block everything in handler */
		die_with_error("sigfillset() failed");
	myAction.sa_flags = 0;

	sigaction(SIGALRM, &myAction, 0);

	int sock; /* Socket */
	struct sockaddr_in server_addr; /* Local address */
	struct sockaddr_in client_addr; /* Client address */
	unsigned int client_addr_len; /* Length of incoming message */
	unsigned short server_port; /* Server port */
	int recv_msg_size; /* Size of received message */

	server_port = 8080; /* First arg:  local port */

	/* Create socket for sending/receiving datagrams */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		die_with_error("socket() failed");

	/* Construct local address structure */
	memset(&server_addr, 0, sizeof(server_addr)); /* Zero out structure */
	server_addr.sin_family = AF_INET; /* Internet address family */
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	server_addr.sin_port = htons(server_port); /* Local port */

	/* Bind to the local address */
	if (bind(sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
		die_with_error("bind() failed");

	for (;;) /* Run forever */
	{
		/* Set the size of the in-out parameter */
		client_addr_len = sizeof(client_addr);

		packet *packet_filename = new packet;

		/* Block until receive message from a client */
		while ((recv_msg_size = recvfrom(sock, packet_filename, MAX_PACKET_SIZE,
				0, (struct sockaddr *) &client_addr, &client_addr_len)) < 0)
			die_with_error("recvfrom() failed");

		printf("Handling client %s\n", inet_ntoa(client_addr.sin_addr));

		FILE* fp = fopen(packet_filename->data, "r");
		uint seq_no = 0;
		printf("reading file name %s\n", packet_filename->data);

		if (fp == NULL) {
			// error
			die_with_error("Error no file found with that name");
			//TODO close connection
		} else {
			packet *data_packet = new packet;

			int n = 100;

			// simulation only
			int simulate = 0;

			// read the file to the end
			// send multiple times
			while ((n = fread(data_packet->data, sizeof(char), CHUNK_SIZE, fp))
					> 0) {

				// ok means that the chunk was sent successfully
				bool ok = false;
				while (!ok) {
					data_packet->seqno = seq_no;
					seq_no = (seq_no + 1) % 2;
					data_packet->len = n; // number of read bytes

					simulate++;
					if (simulate % 100 != 0) {
						/* Send file chunk to the client */
						if (sendto(sock, data_packet, MAX_PACKET_SIZE, 0,
								(struct sockaddr *) &client_addr,
								sizeof(client_addr)) != MAX_PACKET_SIZE)
							die_with_error(
									"sendto() failed with wrong bytes sent");
					}

					// run the timer just after sending the packet
					alarm(TIMEOUT);

					// wait for ack from the client
					ack_packet * ack = new ack_packet;
					while ((recv_msg_size = recvfrom(sock, ack, ACK_PACKET_SIZE,
							0, (struct sockaddr *) &client_addr,
							&client_addr_len)) < 0) {
						if (errno == EINTR) {
							cout << "Time out, resend packt no "
									<< (data_packet->seqno) << endl;
							sendto(sock, data_packet, MAX_PACKET_SIZE, 0,
									(struct sockaddr *) &client_addr,
									sizeof(client_addr));
							// reinitialize the timer
							alarm(TIMEOUT);
						}
					}

					// now ack has been received
					cout << "ack received, ack no = " << ack->ackno << endl;
					if (ack->ackno == seq_no) {
						ok = true;
						alarm(0);
					} else {
						// seqno has been incremented already above :)
						// if ackno == seqno, then its a -ve ack, resend the chunk
						ok = false;
						seq_no = 1 - seq_no;
						cout << "resending packet no " << seq_no << endl;
					}
				}

				data_packet = new packet;
			}

			//send fin packet
			data_packet = new packet;
			data_packet->len = FIN;

			if (sendto(sock, data_packet, MAX_PACKET_SIZE, 0,
					(struct sockaddr *) &client_addr,
					sizeof(client_addr)) != MAX_PACKET_SIZE)
				die_with_error("sendto() failed with wrong bytes sent");

			//close file
			fclose(fp);
		}
	}

	/* NOT REACHED */
}
