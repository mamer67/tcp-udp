#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <iostream>
#include <pthread.h>

#include <errno.h>      /* for errno and EINTR */
#include <signal.h>     /* for sigaction() */
#include <time.h>

#define MAX_PACKET_SIZE                 512     /* Longest string to echo */
#define ACK_PACKET_SIZE                 8
#define CHUNK_SIZE                      504
#define FIN                             2000
#define TIMEOUT                         2

#define ACK								 1
#define NOTACK							 2
#define NOTPCK							 3

using namespace std;

/* Data-only packets */
struct packet {
	/* Header */
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t seqno;
	/* Data */
	char data[CHUNK_SIZE];
};

/* Ack-only packets are only 8 bytes */
struct ack_packet {
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t ackno;
};

FILE* fp;
int sock; /* Socket */
struct sockaddr_in client_addr; /* Client address */
unsigned int client_addr_len; /* Length of incoming message */
uint seqno;
uint window_size;
// array which contains the received data packets
packet ** _buffer;
// array which tell if a packet is ack or not yet
int *ack_window;

bool file_finished;
uint finished_pck;

int *timeout_a;
uint base;

bool inside_window(uint num, uint bases, uint end) {
	if (bases < end) {
		if (num >= bases && num <= end)
			return true;
		else
			return false;
	} else if (bases > end) {
		if (num >= bases || num <= end)
			return true;
		else
			return false;
	} else {
		return false;
	}
}

void die_with_error(const char *error_message) {
	printf("%s\n", error_message);
}

void catch_alarm(int ignored) /* Handler for SIGALRM */
{
	// means that the packet is in the range of the window
	uint end = (base + window_size) % (2 * window_size);

	for (uint i = 0; i < 2 * window_size; i++) {
		timeout_a[i]--;

		if (timeout_a[i] <= 0&& inside_window(i, base, end)
		&& ack_window[i] == NOTACK) {
			printf("alarm time out occurred with pck %d\n", i);
			printf("resend pck %d\n", i);

			// resend the packet
			if (sendto(sock, _buffer[i], MAX_PACKET_SIZE, 0,
					(struct sockaddr *) &client_addr,
					client_addr_len) != MAX_PACKET_SIZE)
				perror("sendto() failed with wrong bytes sent");

			timeout_a[i] = TIMEOUT;
		}
	}

	if (file_finished && ack_window[finished_pck] == ACK) {
		// stop the alarm all packets are sent
	} else {
		alarm(1);
	}
}

bool read_chunk() {
	int n;
	packet *data_packet = new packet;

	if ((n = fread(data_packet->data, sizeof(char), CHUNK_SIZE, fp)) > 0) {

		data_packet->seqno = seqno;
		data_packet->len = n; // number of read bytes

		// put in buffer
		_buffer[seqno] = data_packet;
		ack_window[seqno] = NOTACK;
		timeout_a[seqno] = TIMEOUT;

		seqno = (seqno + 1) % (2 * window_size);

//		if () { // for simulation
		/* Send file chunk to the client */
		if (sendto(sock, data_packet, MAX_PACKET_SIZE, 0,
				(struct sockaddr *) &client_addr,
				client_addr_len) != MAX_PACKET_SIZE)
			die_with_error("sendto() failed with wrong bytes sent");
//		}
		return true;
	} else {
		if (file_finished == false) {
			printf("file finished\n");

			//send fin packet
			data_packet = new packet;
			data_packet->len = FIN;

			// put in buffer
			_buffer[seqno] = data_packet;
			ack_window[seqno] = NOTACK;
			timeout_a[seqno] = TIMEOUT;

			file_finished = true;
			finished_pck = seqno;

			// no need to increment anymore
			// seqno = (seqno + 1) % (2 * window_size);

			if (sendto(sock, data_packet, MAX_PACKET_SIZE, 0,
					(struct sockaddr *) &client_addr,
					client_addr_len) != MAX_PACKET_SIZE)
				die_with_error("sendto() failed with wrong bytes sent");

			//close file
			fclose(fp);
		}
		return false;
	}
}

void* receive_thread(void*) {

	while (1) {
		ack_packet * ack = new ack_packet;
		int n = recvfrom(sock, ack, ACK_PACKET_SIZE, 0,
				(struct sockaddr *) &client_addr, &client_addr_len);

		if (n < 0)
			perror("recvfrom() failed");
		else {
			// now ack has been received
			cout << "ack received, ack no = " << ack->ackno << endl;

			// means that the packet is in the range of the window
			uint end = (base + window_size) % (2 * window_size);

			if (inside_window(ack->ackno, base,
					end) && ack_window[ack->ackno]==NOTACK) {
				ack_window[ack->ackno] = ACK;

				if (ack->ackno == base) {
					int counter = base;

					while (ack_window[counter] == ACK) {
						// read new packets
						read_chunk();
						counter = (counter + 1) % (2 * window_size);
					}

					// update the base
					base = counter;
				}
			}
		}

		//sleep(10);
	}

	return 0;
}

int main(int argc, char *argv[]) {
	// timer initialization
	struct sigaction myAction; /* For setting signal handler */

	/* Set signal handler for alarm signal */
	myAction.sa_handler = catch_alarm;
	if (sigfillset(&myAction.sa_mask) < 0) /* block everything in handler */
		die_with_error("sigfillset() failed");
	myAction.sa_flags = 0;

	sigaction(SIGALRM, &myAction, 0);

	struct sockaddr_in server_addr; /* Local address */
	unsigned short server_port; /* Server port */
	int recv_msg_size; /*Size of received message*/

	window_size = 5;
	server_port = 8080; /* First arg:  local port */

	/* Create socket for sending/receiving datagrams */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		die_with_error("socket() failed");

	/* Construct local address structure */
	memset(&server_addr, 0, sizeof(server_addr)); /* Zero out structure */
	server_addr.sin_family = AF_INET; /* Internet address family */
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	server_addr.sin_port = htons(server_port); /* Local port */

	/* Bind to the local address */
	if (bind(sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
		die_with_error("bind() failed");

//	for (;;) /* Run forever */
//	{
	/* Set the size of the in-out parameter */
	client_addr_len = sizeof(client_addr);

	packet *packet_filename = new packet;

	/* Block until receive message from a client */
	while ((recv_msg_size = recvfrom(sock, packet_filename, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &client_addr, &client_addr_len)) < 0)
		die_with_error("recvfrom() failed");

	printf("Handling client %s\n", inet_ntoa(client_addr.sin_addr));

	fp = fopen(packet_filename->data, "r");
	printf("reading file name %s\n", packet_filename->data);

	if (fp == NULL) {
		// error
		die_with_error("Error no file found with that name");
		//TODO close connection
	} else {
		// start the receiving thread here
		pthread_t pth;
		pthread_create(&pth, NULL, &receive_thread, NULL);

		// array which contains the received data packets
		_buffer = new packet*[2 * window_size];
		// array which tell if a packet is ack or not yet
		ack_window = new int[2 * window_size];
		timeout_a = new int[2 * window_size];
		seqno = 0;
		file_finished = false;
		base = 0;

		alarm(1);

		for (uint i = 0; i < window_size; i++) {
			read_chunk();
		}

		pthread_join(pth, NULL);
	}
//	}

	/* NOT REACHED */
}
