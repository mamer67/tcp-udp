#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <string>
#include <iostream>
#include <signal.h>
#include <time.h>

#include <errno.h>      /* for errno and EINTR */

#define MAX_PACKET_SIZE                 512     /* Longest string to echo */
#define ACK_PACKET_SIZE                 8
#define CHUNK_SIZE                      504
#define FIN                             2000
#define TIMEOUT                         2

using namespace std;

struct packet {
	/* Header */
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t seqno;
	/* Data */
	char data[CHUNK_SIZE]; /* Not always 500 bytes, can be less */
};

/* Ack-only packets are only 8 bytes */
struct ack_packet {
	uint16_t cksum; /* Optional bonus part */
	uint16_t len;
	uint32_t ackno;
};

void die_with_error(const char* error_message) {
	printf("%s\n", error_message);
}

void catch_alarm(int ignored) {
	printf("alarm occurred\n");
}

int main(int argc, char *argv[]) {
	// timer initialization
	struct sigaction myAction; /* For setting signal handler */

	/* Set signal handler for alarm signal */
	myAction.sa_handler = catch_alarm;
	if (sigfillset(&myAction.sa_mask) < 0) /* block everything in handler */
		die_with_error("sigfillset() failed");
	myAction.sa_flags = 0;

	sigaction(SIGALRM, &myAction, 0);

	int sock; /* Socket descriptor */
	struct sockaddr_in server_addr; /* Echo server address */
	struct sockaddr_in from_addr; /* Source address of echo */
	unsigned short server_port; /* Echo server port */
	unsigned int from_size; /* In-out of address size for recvfrom() */
	char *server_ip; /* IP address of server */
	char *filename; /* File name to send to server */
	int filename_len; /* Length of filename */

	server_ip = (char*) "127.0.0.1";
	server_port = 8080;

	filename = (char*) "image.jpg";

	if ((filename_len = strlen(filename)) > CHUNK_SIZE) /* Check input length */
		die_with_error("file name too long");

	packet *filename_packet = new packet;

	//set filename
	strncpy(filename_packet->data, filename, filename_len);
	filename_packet->len = filename_len;
	filename_packet->seqno = 0; // let seqno = 0 be the first name packet
	//TODO checksum
//      filename_packet->cksum;

	/* Create a datagram/UDP socket */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		die_with_error("socket() failed");

	/* Construct the server address structure */
	memset(&server_addr, 0, sizeof(server_addr)); /* Zero out structure */
	server_addr.sin_family = AF_INET; /* Internet addr family */
	server_addr.sin_addr.s_addr = inet_addr(server_ip); /* Server IP address */
	server_addr.sin_port = htons(server_port); /* Server port */

	//TODO timeout
	/* Send the string to the server */
	if (sendto(sock, filename_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &server_addr,
			sizeof(server_addr)) != MAX_PACKET_SIZE)
		die_with_error("sendto() failed with wrong bytes sent");

	/* Recv a response */
	from_size = sizeof(from_addr);

	packet *data_packet = new packet;
	uint expected_seq_no = 0;
	int packet_size = 0;

	//open new file
	FILE* fp = fopen(filename, "w");

	alarm(TIMEOUT);

	while ((packet_size = recvfrom(sock, data_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &from_addr, &from_size)) < 0) {
		if (errno == EINTR) {
			cout << "Time out, resend file name to the server" << endl;
			sendto(sock, filename_packet, MAX_PACKET_SIZE, 0,
					(struct sockaddr *) &server_addr, sizeof(server_addr));
			// reinitialize the timer
			alarm(TIMEOUT);
		}
	}

	int simulate = 0;

	do {
		alarm(0);
		cout << "data: " << data_packet->data << endl;
		cout << "data seqno: " << data_packet->seqno << endl;
		cout << "data length: " << data_packet->len << endl;

		if (data_packet->len == FIN)
			break;

		ack_packet* ack = new ack_packet;
		if (data_packet->seqno == expected_seq_no) {
			expected_seq_no = (data_packet->seqno + 1) % 2;
			// send +ve ack
			ack->ackno = expected_seq_no;

			//write to file
			if ((fwrite(data_packet->data, sizeof(char), data_packet->len, fp))
					< 0) {
				die_with_error("Error writing to file");
			}
		} else {
			// send -ve ack
			ack->ackno = expected_seq_no;
		}

		simulate++;
		if (simulate % 50 != 0) {
			if (sendto(sock, ack, ACK_PACKET_SIZE, 0,
					(struct sockaddr *) &server_addr,
					sizeof(server_addr)) != ACK_PACKET_SIZE) {
				die_with_error("sendto() failed with wrong bytes sent");
			}
		}

		cout << endl;

	} while ((packet_size = recvfrom(sock, data_packet, MAX_PACKET_SIZE, 0,
			(struct sockaddr *) &from_addr, &from_size)) > 0);

	// close file
	fclose(fp);

	if (packet_size < 0) {
		die_with_error("Error in reading datagram");
	}

	if (server_addr.sin_addr.s_addr != from_addr.sin_addr.s_addr) {
		fprintf(stderr, "Error: received a packet from unknown source.\n");
		exit(1);
	}

	close(sock);
	exit(0);
}
