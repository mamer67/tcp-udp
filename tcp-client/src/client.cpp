#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <sys/timeb.h>

// BUFFER_SIZE should never exceed 255, as read, write to socket methods its maximum limit is 255
#define BUFFER_SIZE 255
#define MAX_THREAD 10

using namespace std;

// call back function to handle error
void error(const char *msg) {
	perror(msg);
	exit(0);
}

struct obj {
	string server_file;
	struct sockaddr_in serv_addr;
};

void* fetch_from_server(void* parm) {
	char buffer[BUFFER_SIZE];
	int n;

	obj * pr = (obj*) parm;
	sockaddr_in serv_addr = pr->serv_addr;
	string filename = pr->server_file;

	// get file name
	int startIndex = 0;
	string name = "";
	for (unsigned int i = 0; i < filename.length(); i++) {
		if (filename[i] == '/') {
			startIndex = i;
		}
	}

	if (startIndex == 0)
		startIndex = -1;

	for (unsigned int i = startIndex + 1; i < filename.length(); i++) {
		name += filename[i];
	}
	//----

	printf("client fetch file called %s\n", name.c_str());

	// initialize new network, stream socket, with default protocol
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0)
		error("ERROR opening socket");

	// connect to the server
	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR connecting");

	// write GET request
	string header = "GET /" + filename + " HTTP/1.0\r\n\r\n";
	printf("Sent to server %s\n", header.c_str());

	// write to the socket
	n = write(sockfd, header.c_str(), BUFFER_SIZE);
	if (n < 0)
		error("ERROR writing to socket");

	// open new file
	FILE* pfile;
	pfile = fopen(name.c_str(), "wb");

	// reset buffer & read header
	bzero(buffer, BUFFER_SIZE);
	n = read(sockfd, buffer, BUFFER_SIZE);

	printf("%s\n", buffer);

	// reset buffer
	bzero(buffer, BUFFER_SIZE);
	// read response back from server
	while ((n = read(sockfd, buffer, BUFFER_SIZE)) > 0) {
		printf("%s size= %d\n", name.c_str(), n);
		fwrite(buffer, sizeof(char), n, pfile);
		// reset buffer
		bzero(buffer, BUFFER_SIZE);
	}

	if (n < 0)
		error("ERROR reading from socket");

	// close socket
	close(sockfd);

	// close file
	fclose(pfile);
}

int main(int argc, char *argv[]) {
	ifstream fileio;
	string server_file;
	string file_name = "";
	int sockfd, portno;
	pthread_t pth[MAX_THREAD];

	// server address
	struct sockaddr_in serv_addr;
	struct hostent *server;

	if (argc < 3) {
		fprintf(stderr, "ERROR, must provide address port\n");
		exit(1);
	}

	portno = atoi(argv[2]);

	// set hostname
	server = gethostbyname(argv[1]);

	// check hostname
	if (server == NULL) {
		fprintf(stderr, "ERROR, no such host\n");
		exit(0);
	}

	// initialize serv_addr by zeros
	bzero((char *) &serv_addr, sizeof(serv_addr));

	// assign socket type, addr, port to serv_addr structure
	serv_addr.sin_family = AF_INET;
	bcopy((char *) server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);

	// open local file
	fileio.open("local.txt");

	// get filename to request from server
	getline(fileio, server_file);

	printf("client fetch file called %s\n", server_file.c_str());

	// close file
	fileio.close();

	obj* parm = new obj;
	parm->serv_addr = serv_addr;
	parm->server_file = server_file;

	// get request file from server
	fetch_from_server(parm);

	// open response file to fetch other files
	fileio.open(server_file.c_str());

	int i = 0;

	// loop on file names in response file
	while (!fileio.eof()) {
		// each line is a path for a file on the server
		getline(fileio, file_name);

		printf("client fetch file called %s\n", file_name.c_str());

		parm = new obj;
		parm->serv_addr = serv_addr;
		parm->server_file = file_name;

		//create thread and pass the parameters
		pthread_create(&pth[i], NULL, &fetch_from_server, parm);
		i++;

		// fetch file from server
		// fetch_from_server(parm);
	}

	for (int j = 0; j < i; ++j) {
		//check if any thread doesn't terminate probably
		if (pthread_join(pth[j], NULL) != 0)
			printf("Something went wrong with collecting thread %d", j);
	}

	// close file
	fileio.close();

	return 0;
}
