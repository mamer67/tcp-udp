/* Creates a datagram server.  The port
 number is passed as an argument.  This
 server runs forever */

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/timeb.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <string>
#include <iostream>
#include <signal.h>
#include <time.h>

void error(const char *msg) {
	perror(msg);
	exit(0);
}

int sock, length, n;
socklen_t fromlen;
struct sockaddr_in server;
struct sockaddr_in from;
char buf[1024];

void* print(void*) {
	n = recvfrom(sock, buf, 1024, 0, (struct sockaddr *) &from, &fromlen);
	if (n < 0)
		error("recvfrom");
	write(1, "Received a datagram: ", 21);
	write(1, buf, n);

	return 0;
}

void catch_alarm(int ignored) {
	printf("alarm occurred\n");
}

// timer initialization
struct sigaction myAction; /* For setting signal handler */

void* alr(void* arg) {
	int *num = (int*) arg;

	/* Set signal handler for alarm signal */
	myAction.sa_handler = catch_alarm;
	sigfillset(&myAction.sa_mask);
	myAction.sa_flags = 0;
	sigaction(SIGALRM, &myAction, 0);

	alarm(*num);
	return 0;
}

int main(int argc, char *argv[]) {

//   if (argc < 2) {
//      fprintf(stderr, "ERROR, no port provided\n");
//      exit(0);
//   }
	pthread_t pth;
	pthread_t pth2, pth3, pth4;

	int *nnn = new int;
	*nnn = 5;
	pthread_create(&pth2, NULL, &alr, nnn);
	*nnn = 10;
	pthread_create(&pth3, NULL, &alr, nnn);
	*nnn = 15;
	pthread_create(&pth4, NULL, &alr, nnn);

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		error("Opening socket");
	length = sizeof(server);
	bzero(&server, length);
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(9080/*atoi(argv[1])*/);
	if (bind(sock, (struct sockaddr *) &server, length) < 0)
		error("binding");
	fromlen = sizeof(struct sockaddr_in);

	pthread_create(&pth, NULL, &print, NULL);
	while (1) {

		n = sendto(sock, "Got your message\n", 17, 0, (struct sockaddr *) &from,
				fromlen);
		if (n < 0) {
//				error("sendto");
//			printf("send\n");
		} else {
			printf("send\n");
		}
		sleep(5);
	}

	pthread_join(pth, NULL);
	pthread_join(pth2, NULL);
	pthread_join(pth3, NULL);
	pthread_join(pth4, NULL);
	return 0;
}
